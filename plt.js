function makeArr(startValue, stopValue, cardinality) {
    var arr = [];
    var step = (stopValue - startValue) / (cardinality - 1);
    for (var i = 0; i < cardinality; i++) {
        arr.push(startValue + (step * i));
    }
    return arr;
}

function scaleY(xyvalue)   {
    const yval = xyvalue.map(d => d.yval);
    const ymax = Math.max.apply(null, yval);
    const ymin = Math.min.apply(null, yval);
    const range = (ymax - ymin)/5;
    const ydom = [ymax + range, ymin - range];
    yScale.domain(ydom);
}
const margin = { top: 100, right: 75, bottom: 100, left: 75 }
const width = window.innerWidth - margin.left - margin.right;
const height = window.innerHeight - margin.top - margin.bottom;
const data = makeArr(0, width, 10000);

const zoom = d3.zoom()
    .on('zoom', (event) => {
        const xdom = event.transform.rescaleX(xScale).domain();
        xScale.domain(xdom);

        xy = data.map((d) => {return getXYval(d);} );
        scaleY(xy);
        ypix = xy.map((o) => {return yScale(o.yval);} );
        // const ydom = event.transform.rescaleX(yScale).domain();

        svg.select(".line")
            .attr("d", line);

        svg.select(".x-axis")
            .call(d3.axisBottom(xScale));
        
        svg.select(".y-axis")
            .call(d3.axisLeft(yScale));
    })
    .scaleExtent([0.1, 10]);

const xScale = d3.scaleLinear()
    .domain([-1, 1])
    .range([0, width]);

const yScale = d3.scaleLinear().range([height, 0]);

function getXYval(d) {
    const xval = xScale.invert(d);
    const yval = Math.cos(2* Math.PI *xval);
    return {
        xval,
        yval
    };
}

let xy = data.map((d) => {return getXYval(d);} );
scaleY(xy);
let ypix = xy.map((d) => {return yScale(d.yval);} );

const line = d3.line()
    .x(function (d, ind) { return d})
    .y(function (d, ind) { return ypix[ind] });

const svg = d3.select("#my_chart").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .call(zoom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

svg.append("g")
    .attr("class", "x-axis")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(xScale));

svg.append("g")
    .attr("class", "y-axis")
    // .attr("transform", "translate(" + width + ", 0)")
    .call(d3.axisLeft(yScale));

svg.append("path")
    .datum(data)
    .attr("class", "line")
    .attr("d", line);